import express from "express";

const app = express();

app.get("/", function (req, res) {
  res.send("Hello Algo and DS!");
});

app.listen(3000, function () {
  console.log("Listening on port 3000!");
});
